package ru.sbrf.edu;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class CustomArrayImpl<E> implements CustomArray<E> {

    private int size;
    E[] elementData;
    private static int DEFAULT_CAPACITY = 10;


    public CustomArrayImpl() {
        this(DEFAULT_CAPACITY);
    }

    public CustomArrayImpl(int capacity) {
        elementData = (E[]) new Object[capacity];
    }

    public CustomArrayImpl(Collection<E> collection) {
        this();
        addAll(collection);
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void ensureCapacity(int capacity) {
        elementData = Arrays.copyOf(elementData, elementData.length + capacity);
    }

    public int getCapacity() {
        return elementData.length;
    }

    /*public int calculateCapacity(int capacity) {
        if(capacity <= DEFAULT_CAPACITY) {
            if(size == elementData.length) return DEFAULT_CAPACITY;
        } else {
            int cap = capacity + DEFAULT_CAPACITY;
            while(cap % 10 !=0) {
                cap++;
            }
            return cap;
        }
        return elementData.length;
    }*/

    public boolean add(E item) {
        if (item == null) throw new IllegalArgumentException("Input item is null");
        boolean result = false;
        if (size == elementData.length) {
            elementData = Arrays.copyOf(elementData, elementData.length + DEFAULT_CAPACITY);
            elementData[size++] = item;
            result = true;
        } else {
            elementData[size++] = item;
            result = true;
        }
        return result;
    }

    public boolean addAll(E[] items) {
        boolean result = false;
        for (E item : items) {
            result = add(item);
        }
        return result;
    }

    public boolean addAll(Collection<E> items) {
        boolean result = false;
        Iterator<E> it = items.iterator();
        while (it.hasNext()) {
            E item = it.next();
            result = add(item);
        }
        return result;
    }

    public boolean addAll(int index, E[] items) {
        if (index > size)
            throw new ArrayIndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        int oldSize = size;
        addAll(items);
        Object[] resultArray = new Object[elementData.length];
        /*Напишем копирование ручками, в перспективе использовать System.arraycopy()*/
        for (int i = 0; i < index; i++) {
            resultArray[i] = elementData[i];
        }

        for (int i = 0; i < size - oldSize; i++) {
            resultArray[index + i] = elementData[oldSize + i];
        }
        int j = 0;
        for (int i = size - oldSize + index; i < size; i++) {
            resultArray[i] = elementData[index + j++];
        }
        elementData = (E[]) resultArray;
        return true;
    }

    public void reverse() {
        Object[] resultArray = new Object[elementData.length];
        for(int i = 0; i < size; i++) {
            resultArray[i] = elementData[size - 1 - i];
        }
        elementData = (E[])resultArray;
    }

    public E get(int index) {
        if (index > size - 1)
            throw new ArrayIndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        return elementData[index];
    }

    public E set(int index, E item) {
        Object[] setArr = {item};
        addAll(index, (E[]) setArr);
        return elementData[index + 1];
    }

    public void remove(int index) {
        if (index >= size)
            throw new ArrayIndexOutOfBoundsException("Index " + index + " out of bounds for length " + size);
        Object[] resultArray = new Object[elementData.length];
        /*Напишем ручками, в перспективе использовать System.arraycopy()*/
        for (int i = 0; i < index; i++) {
            resultArray[i] = elementData[i];
        }
        for (int i = index; i < size; i++) {
            resultArray[i] = elementData[index + i];
        }
        size--;
        elementData = (E[]) resultArray;
    }

    public boolean remove(E item) {
        boolean result = false;
        int i = indexOf(item);
        if(i >= 0) {
            remove(i);
            result = true;
        }
        return result;
    }

    public boolean contains(E item) {
        boolean result = false;
        for (int i = 0; i < size; i++) {
            if (elementData[i].equals(item)) {
                return true;
            }
        }
        return result;
    }

    public int indexOf(E item) {
        int result = -1;
        for (int i = 0; i < size; i++) {
            if (elementData[i].equals(item)) {
                return i;
            }
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[ ");

        if(size > 0) {
            for(int i = 0; i < size; i++) {
                result.append(elementData[i] + " ");
            }
        }

        return result.append("]").toString();

    }

    public Object[] toArray() {
        Object[] resultArray = new Object[size];
        for(int i = 0; i < size; i++) {
            resultArray[i] = elementData[i];
        }
        return resultArray;
    }

}

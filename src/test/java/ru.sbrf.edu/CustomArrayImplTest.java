package ru.sbrf.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class CustomArrayImplTest {

    /**
     * Проверка конструктора по умолчанию от {@link CustomArrayImpl}
     */
    @Test
    public void constructorCollectionIntegerTest() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(3, customArray.size());
        Assert.assertEquals(new Integer(3), customArray.get(2));
    }

    /**
     * Проверка конструктора от {@link CustomArrayImpl}
     * Инициализирует CustomArrayImpl значениями переданной коллекции
     */
    @Test
    public void constructorCollectionStringTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("o", customArray.get(2));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#size()}
     * Возвращает текущее количество элементов
     */
    @Test
    public void sizeTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Assert.assertEquals(0, customArray.size());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#contains(Object)}
     * Если элемент содержится возвращает true иначе false
     */
    @Test
    public void containsTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("o", customArray.get(2));
        Assert.assertEquals(true, customArray.contains("o"));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#indexOf(Object)}
     * Возвращает индекс элемента если он найден, иначе -1
     */
    @Test
    public void indexOfTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("o", customArray.get(2));
        Assert.assertEquals(2, customArray.indexOf("o"));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#ensureCapacity(int)}
     * Увеличивает текущее хранилище элементов на переданный аргумент
     */
    @Test
    public void ensureCapacityTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(10, customArray.getCapacity());
        customArray.ensureCapacity(22);
        Assert.assertEquals(32, customArray.getCapacity());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#getCapacity()}
     * Возвращает ткущую емкость хранилища элементов
     */
    @Test
    public void getCapacityTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(10, customArray.getCapacity());
        customArray.ensureCapacity(22);
        Assert.assertEquals(32, customArray.getCapacity());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#reverse()}
     * Зеркалит элементы в хранилище относительно их расположения
     */
    @Test
    public void reverseTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("M", customArray.get(0));
        customArray.reverse();
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("w", customArray.get(0));
        Assert.assertEquals("o", customArray.get(1));
        Assert.assertEquals("e", customArray.get(2));
        Assert.assertEquals("M", customArray.get(3));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#toString()}
     * Переопределенный метод toString для CustomArrayImpl, если хранилище не пусто
     */
    @Test
    public void toStringTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("[ M e o w ]", customArray.toString());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#toArray()}
     * Возвращает представление элементов хранилища в виде массива типа Object
     */
    @Test
    public void toArrayTest() {
        List<String> list = new ArrayList<>();
        list.add("M");
        list.add("e");
        list.add("o");
        list.add("w");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(list);
        Assert.assertEquals(4, customArray.size());
        Object[] objArr = customArray.toArray();

        Assert.assertEquals(4, objArr.length);
        Assert.assertEquals("M", objArr[0]);
        Assert.assertEquals("e", objArr[1]);
        Assert.assertEquals("o", objArr[2]);
        Assert.assertEquals("w", objArr[3]);

        objArr[3] = "K";
        Assert.assertEquals("w", customArray.get(3));
        Assert.assertEquals("K", objArr[3]);
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#toString()}
     * Переопределенный метод toString для CustomArrayImpl, если хранилище пусто возвращает [ ]
     */
    @Test
    public void toStringisEmptyTest() {
        List<String> list = new ArrayList<>();
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        Assert.assertEquals(0, customArray.size());
        Assert.assertEquals("[ ]", customArray.toString());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#isEmpty()}
     * Если в хранилище нет не одного элемента возвращает true, иначе false
     */
    @Test
    public void isEmptyTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Assert.assertEquals(true, customArray.isEmpty());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#add(Object)}
     * Добавляет в хранилище элемент, ели удалось добавить возвращает true, иначе false
     */
    @Test
    public void addTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(33);
        Assert.assertEquals(1, customArray.size());
        Assert.assertEquals(false, customArray.isEmpty());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#add(Object)}
     * Если добавляемый элемент равен null выбрасывает IllegalArgumentException
     */
    @Test
    public void addExceptionTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        Assert.assertEquals(0, customArray.size());
        Assert.assertEquals(true, customArray.isEmpty());
        try {
            customArray.add(null);
            Assert.fail("Ожидается IllegalArgumentException");
        } catch(IllegalArgumentException ex) {

        }
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#get(int)}
     * Возвращает указанный элемент по переданному индексу в качестве аргумента
     */
    @Test
    public void getTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(577);
        Assert.assertEquals(1, customArray.size());
        Assert.assertEquals(false, customArray.isEmpty());
        Assert.assertEquals(new Integer(577), customArray.get(0));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#get(int)}
     * Если индекс элемента который необходимо найти выходит за количество элементов в хранилище -1
     * выбрасываем ArrayIndexOutOfBoundsException
     */
    @Test
    public void getExceptionTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(577);
        Assert.assertEquals(1, customArray.size());
        Assert.assertEquals(false, customArray.isEmpty());
        try {
            Assert.assertEquals(new Integer(577), customArray.get(1));
            Assert.fail("Ожидается ArrayIndexOutOfBoundsException");
        } catch (ArrayIndexOutOfBoundsException ex) {

        }
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#addAll(Object[])} )}
     * Добавляет в хранилище элементы массива переданного в качестве аргумента
     */
    @Test
    public void addAllTest() {
        Integer[] array = {11, 22, 33, 44, 55, 66, 77, 88, 99, 100, 111};
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(577);
        customArray.addAll(array);
        Assert.assertEquals(12, customArray.size());
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#addAll(Object[])}
     * Если в массиве переданного в качестве аргумента чьи элементы нужно добавить в хранилище
     * встречается элемент равный null выбрасываем IllegalArgumentException
     */
    @Test
    public void addAllExceptionTest() {
        String[] array = {null, "22", "33", "44", "55", "66", "77", null, "99", "100", "111"};
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("577");
        try {
            customArray.addAll(array);
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (IllegalArgumentException ex) {

        }
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#addAll(Collection)}
     * Добавляет элементы коллекции переданной в качестве аргумента в хранилище,
     * в качестве аргумента передана коллекция с типом элементов String
     */
    @Test
    public void addAllCollectionTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        Collection<String> list = new ArrayList<>();
        list.add("11");
        list.add("22");
        list.add("33");
        list.add("44");
        list.add("55");
        list.add("66");
        list.add("77");
        list.add("88");
        list.add("99");
        list.add("100");
        list.add("111");
        customArray.add("577");
        customArray.addAll(list);
        Assert.assertEquals(12, customArray.size());
        Assert.assertEquals("77", customArray.get(7));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#addAll(Collection)}
     * Добавляет элементы коллекции переданной в качестве аргумента в хранилище,
     * в качестве аргумента передана коллекция с типом элементов Integer
     */
    @Test
    public void addAllCollectionSetTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Collection<Integer> set = new HashSet<>();
        set.add(11);
        customArray.add(577);
        customArray.addAll(set);
        Assert.assertEquals(2, customArray.size());
        Assert.assertEquals(new Integer(11), customArray.get(1));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#addAll(Collection)}
     * Если в коллекции переданной в качестве аргумента чьи элементы нужно добавить в хранилище
     * встречается элемент равный null выбрасываем IllegalArgumentException
     */
    @Test
    public void addAllCollectionExceptionTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        Collection<String> set = new HashSet<>();
        set.add(null);
        customArray.add("Hello");
        try {
            customArray.addAll(set);
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (IllegalArgumentException ex) {

        }
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#addAll(int, Object[])}
     * Добавляет элементы коллекции переданной в качестве аргумента в хранилище начиная с индекса index,
     * в качестве аргумента передана коллекция с типом элементов String
     */
    @Test
    public void addAllIndexTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        String[] list = {"l", "l",};
        customArray.add("H");
        customArray.add("e");
        customArray.add("o");
        customArray.add(" ");
        customArray.add("W");
        customArray.add("o");
        customArray.add("r");
        customArray.add("l");
        customArray.add("d");
        customArray.addAll(2, list);
        Assert.assertEquals("l", customArray.get(3));
        Assert.assertEquals("o", customArray.get(4));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#addAll(int, Object[])}
     * Если в коллекции переданной в качестве аргумента чьи элементы нужно добавить в хранилище
     * встречается элемент равный null выбрасываем IllegalArgumentException
     */
    @Test
    public void addAllIndexExceptionTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        String[] list = {"l", null,};
        customArray.add("H");
        customArray.add("e");
        customArray.add("o");
        customArray.add(" ");
        customArray.add("W");
        customArray.add("o");
        customArray.add("r");
        customArray.add("l");
        customArray.add("d");
        try {
            customArray.addAll(2, list);
            Assert.fail("Ожидается IllegalArgumentException");
        } catch (IllegalArgumentException ex) {

        }
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#set(int, Object)}
     * Принимает 2 аргумента, индекс и элемент, устанавливает по переданному индексу в хранилище
     * переданный элемент, сдвигая при этом элементы коллекции начиная с индекса к концу
     * В качестве элемента для добавления по индексу тип String
     */
    @Test
    public void setStringTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        String str = "r";
        customArray.add("W");
        customArray.add("o");
        customArray.add("l");
        customArray.add("d");
        Assert.assertEquals("l",customArray.set(2, str));
        Assert.assertEquals(5, customArray.size());
        Assert.assertEquals("r", customArray.get(2));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#set(int, Object)}
     * Принимает 2 аргумента, индекс и элемент, устанавливает по переданному индексу в хранилище
     * переданный элемент, сдвигая при этом элементы коллекции начиная с индекса к концу
     * В качестве элемента для добавления по индексу тип Integer
     */
    @Test
    public void setIntegerTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer digits = 3;
        customArray.add(1);
        customArray.add(2);
        customArray.add(4);
        customArray.add(5);
        Assert.assertEquals(new Integer(4),customArray.set(2, digits));
        Assert.assertEquals(5, customArray.size());
        Assert.assertEquals(new Integer(3), customArray.get(2));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#set(int, Object)}
     *
     * Принимает 2 аргумента, индекс и элемент, устанавливает по переданному индексу в хранилище
     * переданный элемент, сдвигая при этом элементы коллекции начиная с индекса к концу
     * Если переданный индекс выходит за size(>=) и не равен ему выбрасывает ArrayIndexOutOfBoundsException
     * Если индекс для добавления == size то добавляем в конец
     */
    @Test
    public void setExceptionTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        String str = "r";
        customArray.add("W");
        customArray.add("o");
        customArray.add("l");
        customArray.add("d");
        try {
            customArray.set(5, str);
            Assert.fail("Ожидается ArrayIndexOutOfBoundsException");
        } catch(IndexOutOfBoundsException ex) {

        }
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#remove(int)}
     * Удаляет элемент из хранилища по текущему индексу, оставшиеся элементы хранилища после индекса
     * сдвигаются к началу
     */
    @Test
    public void removeIndexTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(4);
        customArray.add(2);
        customArray.add(3);
        customArray.remove(1);
        Assert.assertEquals(3, customArray.size());
        Assert.assertEquals(new Integer(2), customArray.get(1));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#remove(int)}
     * Если в качестве индекса для удаления передан индекс выходящий за size(>)
     * выбрасываем ArrayIndexOutOfBoundsException
     */
    @Test
    public void removeIndexExceptionTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(4);
        customArray.add(2);
        customArray.add(3);
        try {
            customArray.remove(4);
            Assert.fail("Ожидается ArrayIndexOutOfBoundsException");
        } catch(ArrayIndexOutOfBoundsException ex) {

        }
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#remove(Object)}
     * Принимает в качестве аргумента элемент который необходимо удаляет из хранилища,
     * оставшиеся элементы хранилища сдвигаются к началу
     * В хранилище элементы типа Integer, успешное удаление существующего элемента
     */
    @Test
    public void removeItemSuccessIntegerTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(4);
        customArray.add(2);
        customArray.add(3);
        Integer removeItem = 4;
        Assert.assertEquals(true, customArray.remove(removeItem));
        Assert.assertEquals(3, customArray.size());
        Assert.assertEquals(new Integer(2), customArray.get(1));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#remove(Object)}
     * Принимает в качестве аргумента элемент который необходимо удаляет из хранилища,
     * оставшиеся элементы хранилища сдвигаются к началу
     * В хранилище элементы типа String, успешное удаление существующего элемента
     */
    @Test
    public void removeItemSuccessStringTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("M");
        customArray.add("o");
        customArray.add("y");
        customArray.add("y");
        String removeItem = new String("o");
        Assert.assertEquals(true, customArray.remove(removeItem));
        Assert.assertEquals(3, customArray.size());
        Assert.assertEquals("y", customArray.get(1));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#remove(Object)}
     * Принимает в качестве аргумента элемент который необходимо удаляет из хранилища,
     * оставшиеся элементы хранилища сдвигаются к началу
     * В хранилище элементы типа String, удаление не произошло, элемент в хранилище отсутствует
     */
    @Test
    public void removeItemStringFailTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("M");
        customArray.add("o");
        customArray.add("y");
        customArray.add("y");
        String removeItem = new String("P");
        Assert.assertEquals(false, customArray.remove(removeItem));
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("o", customArray.get(1));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#remove(Object)}
     * Принимает в качестве аргумента элемент который необходимо удаляет из хранилища,
     * оставшиеся элементы хранилища сдвигаются к началу
     * В качестве элемента передан Null, удаления не произошло, возвращаем false
     */
    @Test
    public void removeItemNullTest() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("M");
        customArray.add("o");
        customArray.add("y");
        customArray.add("y");
        String removeItem = null;
        Assert.assertEquals(false, customArray.remove(removeItem));
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals("o", customArray.get(1));
    }

    /**
     * Проверка возвращаемого значения от {@link CustomArrayImpl#remove(Object)}
     * Принимает в качестве аргумента элемент который необходимо удаляет из хранилища,
     * оставшиеся элементы хранилища сдвигаются к началу
     * В хранилище элементы типа Integer, удаление не произошло, элемент в хранилище отсутствует
     */
    @Test
    public void removeItemIntegerFailTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(4);
        customArray.add(2);
        customArray.add(3);
        Integer removeItem = 5;
        Assert.assertEquals(false, customArray.remove(removeItem));
        Assert.assertEquals(4, customArray.size());
        Assert.assertEquals(new Integer(4), customArray.get(1));
    }
}
